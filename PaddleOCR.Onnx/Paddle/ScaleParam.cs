﻿// Copyright (c) 2021 raoyutian Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
using Emgu.CV;
namespace PaddleOCR.Onnx.Paddle
{
 internal   class ScaleParam
    {
        int srcWidth;
        int srcHeight;
        int dstWidth;
        int dstHeight;
        float scaleWidth;
        float scaleHeight;

        public int SrcWidth { get => srcWidth; set => srcWidth = value; }
        public int SrcHeight { get => srcHeight; set => srcHeight = value; }
        public int DstWidth { get => dstWidth; set => dstWidth = value; }
        public int DstHeight { get => dstHeight; set => dstHeight = value; }
        public float ScaleWidth { get => scaleWidth; set => scaleWidth = value; }
        public float ScaleHeight { get => scaleHeight; set => scaleHeight = value; }

        public ScaleParam(int srcWidth, int srcHeight, int dstWidth, int dstHeight, float scaleWidth, float scaleHeight)
        {
            this.srcWidth = srcWidth;
            this.srcHeight = srcHeight;
            this.dstWidth = dstWidth;
            this.dstHeight = dstHeight;
            this.scaleWidth = scaleWidth;
            this.scaleHeight = scaleHeight;
        }

      

        public static ScaleParam GetScaleParam(Mat src, int dstSize)
        {
            int srcWidth, srcHeight, dstWidth, dstHeight;
            srcWidth = src.Cols;
            dstWidth = src.Cols;
            srcHeight = src.Rows;
            dstHeight = src.Rows;

            float scale = 1.0F;
            if (dstWidth > dstHeight)
            {
                scale = (float)dstSize / (float)dstWidth;
                dstWidth = dstSize;
                dstHeight = (int)((float)dstHeight * scale);
            }
            else
            {
                scale = (float)dstSize / (float)dstHeight;
                dstHeight = dstSize;
                dstWidth = (int)((float)dstWidth * scale);
            }
            if (dstWidth % 32 != 0)
            {
                dstWidth = (dstWidth / 32 - 1) * 32;
                dstWidth = Math.Max(dstWidth, 32);
            }
            if (dstHeight % 32 != 0)
            {
                dstHeight = (dstHeight / 32 - 1) * 32;
                dstHeight = Math.Max(dstHeight, 32);
            }
            float scaleWidth = (float)dstWidth / (float)srcWidth;
            float scaleHeight = (float)dstHeight / (float)srcHeight;
            return new ScaleParam(srcWidth, srcHeight, dstWidth, dstHeight, scaleWidth, scaleHeight);
        }

    }
}